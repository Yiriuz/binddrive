package com.dani3l11.carfleet.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dani3l11.carfleet.R;
import com.dani3l11.carfleet.data.model.Maintenance;
import com.dani3l11.carfleet.data.repository.MaintenanceRepository;

import java.nio.charset.MalformedInputException;
import java.util.ArrayList;

public class MaintenanceAdapter extends RecyclerView.Adapter<MaintenanceAdapter.ViewHolder> {

    private ArrayList<Maintenance> list;
    private OnManageMaintenanceListener listenerManage;

    public interface OnManageMaintenanceListener{
        void onEditMaintenance(Maintenance maintenance);
        void onDeleteMaintenance(Maintenance maintenance);
    }

    public void setOnManageMaintenance(OnManageMaintenanceListener listenerManage){
        this.listenerManage = listenerManage;
    }

    @NonNull
    @Override
    public MaintenanceAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.maintance_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MaintenanceAdapter.ViewHolder holder, int position) {
        holder.tvDate.setText(list.get(position).getDate());
        holder.tvWorkShop.setText(list.get(position).getClass().toString());
        holder.tvPrice.setText(list.get(position).getPrice().toString());
        holder.bind(position, listenerManage);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvDate;
        TextView tvWorkShop;
        TextView tvPrice;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvDate = itemView.findViewById(R.id.tvDateMaintance);
            tvWorkShop = itemView.findViewById(R.id.tvTypeMaintance);
            tvPrice = itemView.findViewById(R.id.tvPriceMaintance);
        }

        public void bind(final int position, final OnManageMaintenanceListener listenerManage) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listenerManage.onEditMaintenance(list.get(position));
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {

                    listenerManage.onDeleteMaintenance(list.get(position));
                    return true;
                }
            });
        }
    }

    public MaintenanceAdapter() {
        list = (ArrayList<Maintenance>) MaintenanceRepository.getInstance().getList();
    }
}
