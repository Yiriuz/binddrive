package com.dani3l11.carfleet.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dani3l11.carfleet.R;
import com.dani3l11.carfleet.data.model.Car;
import com.dani3l11.carfleet.data.repository.CarRepository;
import com.github.ivbaranov.mli.MaterialLetterIcon;

import java.util.ArrayList;
import java.util.List;

public class CarAdapter extends RecyclerView.Adapter<CarAdapter.ViewHolder> {

    private ArrayList<Car> list;
    private onItemClickListener listenerClick;
    private OnManageCarListener listenerManage;

    public interface OnManageCarListener {
        void onEditCar(Car car);
        void onDeleteCar(Car car);
    }

    public void setOnManageCar(OnManageCarListener listenerManage){
        this.listenerManage = listenerManage;
    }

    public interface onItemClickListener extends View.OnClickListener{
        @Override
        void onClick(View v);
    }

    @NonNull
    @Override
    public CarAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.car_item, parent, false);
        view.setOnClickListener(listenerClick);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CarAdapter.ViewHolder holder, int position) {
        holder.tvName.setText(list.get(position).getName());
        holder.tvBrand.setText(list.get(position).getBrand());
        holder.tvModel.setText(list.get(position).getModel());
        holder.tvKilometers.setText(list.get(position).getKilometers() + "KM");
        holder.pbFuel.setProgress(list.get(position).getFuel());
        holder.bind(position, listenerManage);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvName;
        TextView tvBrand;
        TextView tvModel;
        TextView tvKilometers;
        ProgressBar pbFuel;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvNameCar);
            tvBrand = itemView.findViewById(R.id.tvBrandCar);
            tvModel = itemView.findViewById(R.id.tvModelCar);
            tvKilometers = itemView.findViewById(R.id.tvKilometersCar);
            pbFuel = itemView.findViewById(R.id.pbFuel);
        }

        public void bind(final int position, final OnManageCarListener listenerManage) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listenerManage.onEditCar(list.get(position));
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    listenerManage.onDeleteCar(list.get(position));
                    return true;
                }
            });
        }
    }

    public CarAdapter() {
        list = new ArrayList<>();
    }

    public void delete(Car deleted){
        list.remove(deleted);
    }

    public void clear(){
        list.clear();
    }

    public void load(List<Car> seccionList){
        list.addAll(seccionList);
    }
}
