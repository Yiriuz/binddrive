package com.dani3l11.carfleet.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dani3l11.carfleet.R;
import com.dani3l11.carfleet.data.model.Maintenance;
import com.dani3l11.carfleet.data.model.Refuel;
import com.dani3l11.carfleet.data.model.Travel;
import com.dani3l11.carfleet.data.repository.MaintenanceRepository;
import com.dani3l11.carfleet.data.repository.RefuelRepository;
import com.dani3l11.carfleet.data.repository.TravelRepository;

import java.util.ArrayList;
import java.util.List;

public class HistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int ITEM_TYPE_REFUEL = 0;
    public static final int ITEM_TYPE_MAINTENANCE = 2;
    public static final int ITEM_TYPE_TRAVEL = 4;

    private List<Object> list = new ArrayList<Object>();
    private OnManageItemListener listenerManage;

    public interface OnManageItemListener {
        void onEditRefuel(Refuel refuel);
        void onDeleteRefuel(Refuel refuel);
        void onEditMaintenance(Maintenance maintenance);
        void onDeleteMaintenance(Maintenance maintenance);
        void onEditTravel(Travel travel);
        void onDeleteTravel(Travel travel);
    }

    public void setOnManageRefuel(OnManageItemListener listenerManage){
        this.listenerManage = listenerManage;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       View view = null;
        switch (viewType){
            case ITEM_TYPE_REFUEL:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.refuel_card, parent, false);
                return new RefuelViewHolder(view);
            case ITEM_TYPE_MAINTENANCE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.maintance_card, parent, false);
                return new MaintenanceViewHolder(view);
            case ITEM_TYPE_TRAVEL:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.travel_card, parent, false);
                return new TravelViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()){
            case ITEM_TYPE_REFUEL:
                RefuelViewHolder refuelViewHolder = (RefuelViewHolder)holder;
                refuelViewHolder.tvDate.setText(((Refuel)list.get(position)).getDate());
                refuelViewHolder.tvPlace.setText(((Refuel)list.get(position)).getUbication());
                refuelViewHolder.tvPrice.setText(((Refuel)list.get(position)).getPrice().toString()+"€");
                refuelViewHolder.bind(position, listenerManage);
                break;
            case ITEM_TYPE_MAINTENANCE:
                MaintenanceViewHolder maintenanceViewHolder = (MaintenanceViewHolder)holder;
                maintenanceViewHolder.tvDate.setText(((Maintenance)list.get(position)).getDate());
                maintenanceViewHolder.tvType.setText(((Maintenance)list.get(position)).getType());
                maintenanceViewHolder.tvPrice.setText(((Maintenance)list.get(position)).getPrice().toString()+"€");
                maintenanceViewHolder.bind(position, listenerManage);
                break;
            case ITEM_TYPE_TRAVEL:
                TravelViewHolder travelViewHolder = (TravelViewHolder)holder;
                travelViewHolder.tvDate.setText(((Travel)list.get(position)).getdTravel());
                travelViewHolder.tvPlace1.setText(((Travel)list.get(position)).getStartPlace());
                travelViewHolder.tvPlace2.setText(((Travel)list.get(position)).getEndPlace());
                travelViewHolder.tvKilometers.setText(((Travel)list.get(position)).getKilometers().toString() + " Km");
        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RefuelViewHolder extends RecyclerView.ViewHolder{

        TextView tvDate;
        TextView tvPlace;
        TextView tvPrice;

        public RefuelViewHolder(@NonNull View itemView) {
            super(itemView);
            tvDate = itemView.findViewById(R.id.tvDateRefuel);
            tvPlace = itemView.findViewById(R.id.tvPlaceRefuel);
            tvPrice = itemView.findViewById(R.id.tvPriceRefuel);
        }


        public void bind(final int position, final OnManageItemListener listenerManage) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listenerManage.onEditRefuel(((Refuel)list.get(position)));
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    listenerManage.onDeleteRefuel(((Refuel)list.get(position)));
                    return true;
                }
            });
        }
    }

    public class MaintenanceViewHolder extends RecyclerView.ViewHolder{

        TextView tvDate;
        TextView tvType;
        TextView tvPrice;

        public MaintenanceViewHolder(@NonNull View itemView) {
            super(itemView);
            tvDate = itemView.findViewById(R.id.tvDateMaintance);
            tvType = itemView.findViewById(R.id.tvTypeMaintance);
            tvPrice = itemView.findViewById(R.id.tvPriceMaintance);
        }

        public void bind(final int position, final OnManageItemListener listenerManage) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listenerManage.onEditMaintenance(((Maintenance)list.get(position)));
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    listenerManage.onDeleteMaintenance(((Maintenance)list.get(position)));
                    return true;
                }
            });
        }
    }

    public class TravelViewHolder extends RecyclerView.ViewHolder{

        TextView tvDate;
        TextView tvPlace1;
        TextView tvPlace2;
        TextView tvKilometers;

        public TravelViewHolder(@NonNull View itemView) {
            super(itemView);
            tvDate = itemView.findViewById(R.id.tvDateTravel);
            tvPlace1 = itemView.findViewById(R.id.tvPlaceTravel);
            tvPlace2 = itemView.findViewById(R.id.tvPlaceTravel2);
            tvKilometers = itemView.findViewById(R.id.tvkilometersTravel);
        }

        public void bind(final int position, final OnManageItemListener listenerManage){
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listenerManage.onEditTravel((((Travel)list.get(position))));
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    listenerManage.onDeleteTravel(((Travel)list.get(position)));
                    return true;
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (list.get(position) instanceof Refuel)
            return ITEM_TYPE_REFUEL;
        else if (list.get(position) instanceof Maintenance)
            return ITEM_TYPE_MAINTENANCE;
        else
            return ITEM_TYPE_TRAVEL;
    }

    public HistoryAdapter() {
        list.addAll(RefuelRepository.getInstance().getList());
        list.addAll(MaintenanceRepository.getInstance().getList());
        list.addAll(TravelRepository.getInstance().getList());
    }


    public void load(List<Object> listObject){
        list.addAll(listObject);
    }
}
