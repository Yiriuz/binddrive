package com.dani3l11.carfleet.data.repository;

import com.dani3l11.carfleet.data.model.Refuel;

import java.util.ArrayList;
import java.util.List;

public class RefuelRepository {
    private List<Refuel> list;
    private static RefuelRepository repository;

    static{
        repository = new RefuelRepository();
    }

    public RefuelRepository(){
        list = new ArrayList<>();
        incialite();
    }

    private void incialite() {
        list.add(new Refuel(1, "Shell", "Carlos Haya", 20, "07/01/2020",220.00));
        list.add(new Refuel(2, "M3", "Francisco de Quevedo", 30,"06/01/2020", 121.20));
        list.add(new Refuel(3, "Repsol", "FelipeIV", 30, "23/12/2019",118.21));
        list.add(new Refuel(4, "BP", "Carlos Haya", 10, "02/11/2019",10.21));
        list.add(new Refuel(5, "BP", "Franciso de Quevedo", 10, "02/10/2019",34.12));
    }

    public List<Refuel> getList(){
        return list;
    }

    public static RefuelRepository getInstance(){
        return repository;
    }

    public boolean add(Refuel refuel){
        return list.add(refuel);
    }

    public boolean delete(Refuel refuel){
        return list.remove(list.get(list.indexOf(refuel)));
    }

    public void edit(Refuel refuel){
        int posicion = list.indexOf(refuel);
        list.remove(posicion);
        list.add(posicion, refuel);
    }
}
