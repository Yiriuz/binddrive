package com.dani3l11.carfleet.data.repository;

import com.dani3l11.carfleet.data.model.UserAdmin;
import com.dani3l11.carfleet.data.model.UserStandar;

import java.util.ArrayList;
import java.util.List;

public class UserAdminRepository {

    private List<UserAdmin> list;
    private static UserAdminRepository repository;
    private List<UserStandar> workers;

    static {
        repository = new UserAdminRepository();
    }

    public UserAdminRepository(){
        list = new ArrayList<>();
        workers = UserStandarRepository.getInstance().getList();
        addData();
    }

    private void addData() {
        list.add(new UserAdmin("garciaantoniodaniel11@gmail.com", "Yiriuz",
                                "hide11clock", "Antonio Daniel", "García Guerrero",
                                null, workers, "DaniS.A."));
        list.add(new UserAdmin("agarciafernandez7@gmail.com", "Antuan",
                "relojero11", "Antonio", "García Fernández",
                null, null, "Take S.A."));
    }

    public List<UserAdmin> getList(){
        return list;
    }

    public static UserAdminRepository getInstance(){
        return repository;
    }

    public boolean add(UserAdmin userAdmin){
        return list.add(userAdmin);
    }

    public boolean delete(UserAdmin userAdmin){
        return list.remove(list.get(list.indexOf(userAdmin)));
    }

    public void edit(UserAdmin userAdmin){
        int posicion = list.indexOf(userAdmin);
        list.remove(posicion);
        list.add(posicion, userAdmin);
    }

}
