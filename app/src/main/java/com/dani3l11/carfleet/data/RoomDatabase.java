package com.dani3l11.carfleet.data;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;

import com.dani3l11.carfleet.data.dao.CarDao;
import com.dani3l11.carfleet.data.model.Car;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {Car.class}, version = 1, exportSchema = false)
public abstract class RoomDatabase extends androidx.room.RoomDatabase {

    private static volatile RoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 4;
    public static final ExecutorService databaseWriteExecutor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    static RoomDatabase getDatabase(final Context context){
        if (INSTANCE == null){
            synchronized (RoomDatabase.class){
                if(INSTANCE == null){
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), RoomDatabase.class,
                                                    "binddrivedb").build();
                }
            }
        }
        return INSTANCE;
    }

    public static void create(final Context context){
        if (INSTANCE == null){
            synchronized (RoomDatabase.class){
                if(INSTANCE == null){
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), RoomDatabase.class,
                            "binddrivedb").build();
                }
            }
        }
    }

    public static RoomDatabase getDatabase(){return INSTANCE;}

    public abstract CarDao getCarDao();
}
