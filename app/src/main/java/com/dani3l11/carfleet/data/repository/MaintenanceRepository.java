package com.dani3l11.carfleet.data.repository;

import com.dani3l11.carfleet.data.model.Maintenance;

import java.util.ArrayList;
import java.util.List;

public class MaintenanceRepository {
    private List<Maintenance> list;
    private static MaintenanceRepository repository;

    static {
        repository = new MaintenanceRepository();
    }

    public MaintenanceRepository(){
        list = new ArrayList<>();
        inicialite();
    }

    private void inicialite() {
        list.add(new Maintenance(1, "R.F.A. Brothers", "11/11/2020", "Oil Change", 120.00));
        list.add(new Maintenance(2, "Lenny S.A.", "14/11/2020", "Door Repair", 530.0));
        list.add(new Maintenance(3, "R.F.A. Brothers", "18/11/2020", "Wheel Change", 329.99));
    }

    public List<Maintenance> getList() {return list;}

    public static MaintenanceRepository getInstance() {return repository;}

    public boolean add(Maintenance maintenance) {return list.add(maintenance);}

    public boolean delete(Maintenance maintenance) {
        return list.remove(list.get(list.indexOf(maintenance)));
    }

    public void edit(Maintenance maintenance){
        int position = list.indexOf(maintenance);
        list.remove(position);
        list.add(position, maintenance);
    }
}
