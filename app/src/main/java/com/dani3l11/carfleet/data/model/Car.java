package com.dani3l11.carfleet.data.model;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.Editable;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.Date;
import java.util.Objects;

@Entity
public class Car implements Parcelable {
    @Ignore
    public static final String TAG = "Car";
    @Ignore
    private static Integer idstatic;
    @PrimaryKey
    @NotNull
    private Integer id;
    @NotNull
    private String  name;
    @NotNull
    private String brand;
    @NotNull
    private String model;
    @Nullable
    private Integer kilometers;
    @Nullable
    private String itv;
    @Nullable
    private String vehicleAdquired;
    @Nullable
    private String vehicleBuild;
    @NotNull
    private Integer fuel;

    public Car(Integer id, String name, String brand, String model, Integer kilometers, String itv, String vehicleAdquired, String vehicleBuild, Integer fuel) {
        this.id = id;
        this.name = name;
        this.brand = brand;
        this.model = model;
        this.kilometers = kilometers;
        this.itv = itv;
        this.vehicleAdquired = vehicleAdquired;
        this.vehicleBuild = vehicleBuild;
        this.fuel = fuel;
    }

    @Ignore
    public Car() {

    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Car)) return false;
        Car car = (Car) o;
        return Objects.equals(id, car.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Ignore
    protected Car(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        name = in.readString();
        if (in.readByte() == 0) {
            kilometers = null;
        } else {
            kilometers = in.readInt();
        }
    }



    public static final Creator<Car> CREATOR = new Creator<Car>() {
        @Override
        public Car createFromParcel(Parcel in) {
            return new Car(in);
        }

        @Override
        public Car[] newArray(int size) {
            return new Car[size];
        }
    };

    public Integer getFuel() {
        return fuel;
    }

    public void setFuel(Integer fuel) {
        this.fuel = fuel;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getKilometers() {
        return kilometers;
    }

    public void setKilometers(String kilometers) {
        this.kilometers = Integer.parseInt(kilometers);
    }

    public String getItv() {
        return itv;
    }

    public void setItv(String itv) {
        this.itv = itv;
    }

    public String getVehicleAdquired() {
        return vehicleAdquired;
    }

    public void setVehicleAdquired(String vehicleAdquired) {
        this.vehicleAdquired = vehicleAdquired;
    }

    public String getVehicleBuild() {
        return vehicleBuild;
    }

    public void setVehicleBuild(String vehicleBuild) {
        this.vehicleBuild = vehicleBuild;
    }

    @Override
    public String toString() {
        return "Car{" +
                "name='" + name + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(brand);
        dest.writeString(model);
        dest.writeInt(kilometers);
        dest.writeInt(fuel);
    }

    public static class OrdenarPorNombre implements Comparator {

        @Override
        public int compare(Object o1, Object o2) {
            Car sorteo1 = (Car) o1;
            Car sorteo2 = (Car) o2;

            return sorteo1.getName().compareTo(sorteo2.getName());
        }
    }

    public static class OrdenarPorCombustible implements Comparator{

        @Override
        public int compare(Object o1, Object o2) {
            Car car1 = (Car) o1;
            Car car2 = (Car) o2;

            return car1.getFuel().compareTo(car2.getFuel());
        }
    }
}
