package com.dani3l11.carfleet.data.model;

public class UserStandar extends User {

    private int age;

    public UserStandar(String email, String userName, String password, String name, String surname, String imgUrl, int age) {
        super(email, userName, password, name, surname, imgUrl);
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "UserStandar{" +
                "age='" + age + '\'' +
                '}';
    }
}
