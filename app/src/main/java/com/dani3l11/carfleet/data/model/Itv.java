package com.dani3l11.carfleet.data.model;

import java.util.Date;

public class Itv {
    private Integer id;
    private Date dRevision;
    private Boolean success;
    private Double price;

    public Itv(Integer id, Date dRevision, Boolean success, Double price) {
        this.id = id;
        this.dRevision = dRevision;
        this.success = success;
        this.price = price;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getdRevision() {
        return dRevision;
    }

    public void setdRevision(Date dRevision) {
        this.dRevision = dRevision;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Itv{" +
                "id=" + id +
                ", dRevision=" + dRevision +
                '}';
    }
}
