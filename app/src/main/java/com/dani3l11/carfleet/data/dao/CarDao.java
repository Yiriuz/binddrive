package com.dani3l11.carfleet.data.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.dani3l11.carfleet.data.model.Car;

import java.util.List;

@Dao
public interface CarDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Car car);
    @Delete
    void delete(Car car);
    @Update
    void update(Car car);
    @Query("SELECT * FROM car ORDER BY id")
    List<Car> getAll();
    @Query("SELECT * FROM car WHERE id=:id")
    Car findById(Integer id);
}
