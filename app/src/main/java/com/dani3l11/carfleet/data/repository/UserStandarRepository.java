package com.dani3l11.carfleet.data.repository;

import com.dani3l11.carfleet.data.model.User;
import com.dani3l11.carfleet.data.model.UserStandar;

import java.util.ArrayList;
import java.util.List;

public class UserStandarRepository {

    private List<UserStandar> list;
    private static UserStandarRepository repository;

    static {
        repository = new UserStandarRepository();
    }

    public UserStandarRepository(){
        list = new ArrayList<>();
        addData();
    }

    private void addData() {
        list.add(new UserStandar("alberto20@gmail.com", "Albert", "relojero", "Alberto", "Rodríguez Aguilar", null, 45));
        list.add(new UserStandar("rafa@gmail.com", "Raf", "relojero", "Rafael Manuel", "Cortés García", null, 27));
        list.add(new UserStandar("inma@gmail.com", "Inma", "123456", "Inma", "Guerrero Baena", null, 36));
    }

    public List<UserStandar> getList(){
        return list;
    }

    public static UserStandarRepository getInstance(){
        return repository;
    }

    public boolean add(UserStandar userStandar){
        return list.add(userStandar);
    }

    public boolean delete(UserStandar userStandar){
        return list.remove(list.get(list.indexOf(userStandar)));
    }

    public void edit(UserStandar userStandar){
        int posicion = list.indexOf(userStandar);
        list.remove(posicion);
        list.add(posicion, userStandar);
    }

}
