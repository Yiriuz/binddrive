package com.dani3l11.carfleet.data.repository;

import android.os.AsyncTask;

import com.dani3l11.carfleet.data.RoomDatabase;
import com.dani3l11.carfleet.data.dao.CarDao;
import com.dani3l11.carfleet.data.model.Car;
import com.dani3l11.carfleet.iu.car.CarListPresenter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class CarRepository {
    private CarDao carDao;
    private static CarRepository repository;
    private CarListPresenter.OnSuccessListener onSuccessListenerd;

    static{
        repository = new CarRepository();
    }

    private CarRepository(){
        carDao = RoomDatabase.getDatabase().getCarDao();
    }

    public static CarRepository getRepository(){
        return repository;
    }

    //------------------------------MÉTODOS ASINCORNOS-------------------------//

    private class QueryAsyncTask extends AsyncTask<Void, Void, List<Car>>{

        public QueryAsyncTask(CarListPresenter.OnSuccessListener onSuccessListener){
            onSuccessListenerd = onSuccessListener;
        }

        @Override
        protected List<Car> doInBackground(Void... voids) {
            return carDao.getAll();
        }

        @Override
        protected void onPostExecute(List<Car> cars) {
            super.onPostExecute(cars);
            onSuccessListenerd.onSuccess(cars);
        }
    }


    private class InsertAsyncTask extends AsyncTask<Car, Void, Long>{

        @Override
        protected Long doInBackground(Car... cars) {
            Long result = carDao.insert(cars[0]);

            if(result == -1)
                carDao.update(cars[0]);

            return null;
        }
    }

    //-----------------------------FIN MÉTODOS ASÍNCRONOS----------------------//

    /*public void SortCarByName(){
        Collections.sort(list, new Car.OrdenarPorNombre());
    }

    public void SortCarByFuel(){
        Collections.sort(list, new Car.OrdenarPorCombustible());
    }*/



    public List<Car> getList(){
        List<Car> list = null;

        try{
            list = RoomDatabase.databaseWriteExecutor.submit(() -> carDao.getAll()).get();
            return list;
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static CarRepository getInstance(){
        return repository;
    }

    public long insert(final Car car){
        long rowId = 1;

        try {
            rowId = RoomDatabase.databaseWriteExecutor.submit(() -> carDao.insert(car)).get();
            return rowId;
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public boolean delete(Car car){
        RoomDatabase.databaseWriteExecutor.execute(() -> carDao.delete(car));
        return true;
    }

    public void update(Car car){
        RoomDatabase.databaseWriteExecutor.execute(() -> carDao.update(car));
    }


}
