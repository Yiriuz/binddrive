package com.dani3l11.carfleet.data.model;

import java.util.List;

/**
 * Clase modelo User
 */
public class User {
    private String email;
    private String userName;
    private String password;
    private String name;
    private String surname;
    private String imgUrl;

    public User(String email, String userName, String password, String name, String surname, String imgUrl) {
        this.email = email;
        this.userName = userName;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.imgUrl = imgUrl;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    @Override
    public String toString() {
        return "User{" +
                "email='" + email + '\'' +
                ", userName='" + userName + '\'' +
                '}';
    }

}
