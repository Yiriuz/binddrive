package com.dani3l11.carfleet.data.model;

import android.app.Application;

public class GlobalData extends Application {
    private UserAdmin userLoged;

    public UserAdmin getUserLoged() {
        return userLoged;
    }

    public void setUserLoged(UserAdmin userLoged) {
        this.userLoged = userLoged;
    }
}
