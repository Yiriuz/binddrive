package com.dani3l11.carfleet.data.model;

import java.sql.Date;
import java.sql.Time;

public class Travel {
    private Integer id;
    private String dTravel;
    private String startTime;
    private String dTravelEnd;
    private String endTime;
    private String startPlace;
    private String endPlace;
    private Integer kilometers;

    public Travel(Integer id, String dTravel, String startTime, String dTravelEnd, String endTime, String startPlace, String endPlace, Integer kilometers) {
        this.id = id;
        this.dTravel = dTravel;
        this.startTime = startTime;
        this.dTravelEnd = dTravelEnd;
        this.endTime = endTime;
        this.startPlace = startPlace;
        this.endPlace = endPlace;
        this.kilometers = kilometers;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getdTravel() {
        return dTravel;
    }

    public void setdTravel(String dTravel) {
        this.dTravel = dTravel;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getdTravelEnd() {
        return dTravelEnd;
    }

    public void setdTravelEnd(String dTravelEnd) {
        this.dTravelEnd = dTravelEnd;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getStartPlace() {
        return startPlace;
    }

    public void setStartPlace(String startPlace) {
        this.startPlace = startPlace;
    }

    public String getEndPlace() {
        return endPlace;
    }

    public void setEndPlace(String endPlace) {
        this.endPlace = endPlace;
    }

    public Integer getKilometers() {
        return kilometers;
    }

    public void setKilometers(Integer kilometers) {
        this.kilometers = kilometers;
    }

    @Override
    public String toString() {
        return "Travel{" +
                "id=" + id +
                ", dTravel=" + dTravel +
                ", startPlace='" + startPlace + '\'' +
                '}';
    }
}
