package com.dani3l11.carfleet.data.repository;

import com.dani3l11.carfleet.data.model.Travel;

import java.util.ArrayList;
import java.util.List;

public class TravelRepository {

    private List<Travel> list;
    private static TravelRepository repository;

    static {
        repository = new TravelRepository();
    }

    public TravelRepository(){
        list = new ArrayList<>();
        addData();
    }

    private void addData() {
        list.add(new Travel(1, "12/12/2020", "12:20", "12/12/2020", "15:23", "Puente Genil, Córdoba", "Málaga, Málaga", 23000));
        list.add(new Travel(2, "12/12/2020", "17:20", "12/12/2020", "20:23", "Málaga, Málaga", "Málaga, Málaga", 23000));
        list.add(new Travel(3, "12/19/2020", "12:20", "12/20/2020", "15:23", "Puente Genil, Córdoba", "Madrid, Madrid", 254123));
    }

    public List<Travel> getList() {return list;}

    public static TravelRepository getInstance() {return repository;}

    public boolean add(Travel travel) {return list.add(travel);}

    public boolean delete(Travel travel) {
        return list.remove(list.get(list.indexOf(travel)));
    }

    public void edit(Travel travel){
        int position = list.indexOf(travel);
        list.remove(position);
        list.add(position, travel);
    }
}
