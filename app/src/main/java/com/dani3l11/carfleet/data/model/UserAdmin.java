package com.dani3l11.carfleet.data.model;

import java.util.List;

public class UserAdmin extends User {

    private List<UserStandar> workers;
    private String companyName;

    public UserAdmin(String email, String userName, String password, String name, String surname, String imgUrl, List<UserStandar> workers, String companyName) {
        super(email, userName, password, name, surname, imgUrl);
        this.workers = workers;
        this.companyName = companyName;
    }

    public List<UserStandar> getWorkers() {
        return workers;
    }

    public void setWorkers(List<UserStandar> workers) {
        this.workers = workers;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    @Override
    public String toString() {
        return "UserAdmin{" +
                "workers=" + workers +
                ", companyName='" + companyName + '\'' +
                '}';
    }
}
