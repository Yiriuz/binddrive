package com.dani3l11.carfleet.data.model;

import java.util.Date;

public class Maintenance {
    private Integer id;
    private String name;
    private String date;
    private String type;
    private Double price;

    public Maintenance(Integer id, String name, String date, String type, Double price) {
        this.id = id;
        this.name = name;
        this.date = date;
        this.type = type;
        this.price = price;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Maintenance{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
