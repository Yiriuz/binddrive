package com.dani3l11.carfleet.data.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Refuel implements Parcelable {
    public static final String TAG = "Refuel";
    private Integer id;
    private String gasStation;
    private String ubication;
    private Integer lRefuel;
    private String date;
    private Double price;

    public Refuel(Integer id, String gasStation, String ubication, Integer lRefuel, String date, Double price) {
        this.id = id;
        this.gasStation = gasStation;
        this.ubication = ubication;
        this.lRefuel = lRefuel;
        this.date = date;
        this.price = price;
    }

    protected Refuel(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        gasStation = in.readString();
        ubication = in.readString();
        if (in.readByte() == 0) {
            lRefuel = null;
        } else {
            lRefuel = in.readInt();
        }
        date = in.readString();
        if (in.readByte() == 0) {
            price = null;
        } else {
            price = in.readDouble();
        }
    }

    public static final Creator<Refuel> CREATOR = new Creator<Refuel>() {
        @Override
        public Refuel createFromParcel(Parcel in) {
            return new Refuel(in);
        }

        @Override
        public Refuel[] newArray(int size) {
            return new Refuel[size];
        }
    };

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGasStation() {
        return gasStation;
    }

    public void setGasStation(String gasStation) {
        this.gasStation = gasStation;
    }

    public String getUbication() {
        return ubication;
    }

    public void setUbication(String ubication) {
        this.ubication = ubication;
    }

    public Integer getlRefuel() {
        return lRefuel;
    }

    public void setlRefuel(Integer lRefuel) {
        this.lRefuel = lRefuel;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Refuel{" +
                "id=" + id +
                ", gasStation='" + gasStation + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(gasStation);
        dest.writeString(ubication);
        dest.writeInt(lRefuel);
        dest.writeString(date);
        dest.writeDouble(price);
    }
}
