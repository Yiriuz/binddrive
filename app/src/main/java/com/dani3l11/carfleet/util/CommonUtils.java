package com.dani3l11.carfleet.util;

import java.util.regex.Pattern;

public final class CommonUtils {

    /**
     * This will check if the password is valid
     */
    public static boolean checkPatternPassword(String password){
        final String pattern_password = "(?=.*[0-9])(?=.*[a-z])(?=\\\\S+$).{8,40}";
        Pattern pattern = Pattern.compile(pattern_password);
        return pattern.matcher(password).matches();
    }
}
