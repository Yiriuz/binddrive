package com.dani3l11.carfleet.iu.refuel;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dani3l11.carfleet.R;
import com.dani3l11.carfleet.adapter.HistoryAdapter;
import com.dani3l11.carfleet.data.model.Maintenance;
import com.dani3l11.carfleet.data.model.Refuel;
import com.dani3l11.carfleet.data.model.Travel;
import com.dani3l11.carfleet.iu.BaseDialogFragment;

public class RefuelListFragment extends Fragment implements RefuelListContract.View, BaseDialogFragment.OnFinishDialogListener {
    public static final String TAG = "RefuelListFragment";
    private RecyclerView rvRefuel;
    private HistoryAdapter adapter;
    private HistoryAdapter.OnManageItemListener listenerAdapter;
    private OnManageRefuelListener listenerManage;
    private RefuelListContract.Presenter presenter;
    private static final int CODE_DELETE = 300;
    private Refuel deleted;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list_refuel, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        getActivity().setTitle("History");
        //((AppCompatActivity) getActivity()).getSupportActionBar().hide();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvRefuel = view.findViewById(R.id.rvRefuel);

        initListRefuel();
    }

    private void initListRefuel() {
        initListenerAdapter();

        adapter = new HistoryAdapter();
        adapter.setOnManageRefuel(listenerAdapter);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        rvRefuel.setLayoutManager(linearLayoutManager);

        rvRefuel.setAdapter(adapter);
    }

    private void initListenerAdapter() {
        listenerAdapter = new HistoryAdapter.OnManageItemListener() {
            @Override
            public void onEditRefuel(Refuel refuel) {
                listenerManage.onManageRefuel(refuel);
            }

            @Override
            public void onDeleteRefuel(Refuel refuel) {
                showDialogDelete(refuel);
            }

            @Override
            public void onEditMaintenance(Maintenance maintenance) {
                Toast.makeText(getContext(), "Maintenance Edit", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onDeleteMaintenance(Maintenance maintenance) {
                Toast.makeText(getContext(), "Maintenance Delete! :D", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onEditTravel(Travel travel) {
                Toast.makeText(getContext(), "Travel Edit", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onDeleteTravel(Travel travel) {
                Toast.makeText(getContext(), "Travel Delete", Toast.LENGTH_SHORT).show();
            }
        };
    }

    private void showDialogDelete(Refuel refuel) {
        Bundle bundle = new Bundle();
        bundle.putString(BaseDialogFragment.TITLE, "Eliminar");
        bundle.putString(BaseDialogFragment.MESSAGE, "¿Está seguro de que quiere eliminar este repostaje?");
        BaseDialogFragment baseDialogFragment = BaseDialogFragment.newInstance(bundle);
        baseDialogFragment.setTargetFragment(RefuelListFragment.this, CODE_DELETE);
        baseDialogFragment.show(getFragmentManager(), BaseDialogFragment.TAG);
        deleted = refuel;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            listenerManage = (OnManageRefuelListener) context;
        } catch (ClassCastException e){
            throw new ClassCastException(context.toString());
        }
    }

    public Object newInstance(Bundle bundle) {
        RefuelListFragment carListFragment = new RefuelListFragment();
        if (bundle != null)
            carListFragment.setArguments(bundle);
        return carListFragment;
    }

    @Override
    public void onSuccessDelete() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void setPresenter(RefuelListContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void onSuccess() {

    }

    @Override
    public void onFinishDialog() {
        presenter.delete(deleted);
    }

    public interface OnManageRefuelListener {
        void onManageRefuel(Refuel refuel);
    }

}
