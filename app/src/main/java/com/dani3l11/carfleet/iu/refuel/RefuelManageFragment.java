package com.dani3l11.carfleet.iu.refuel;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dani3l11.carfleet.R;
import com.dani3l11.carfleet.data.model.Refuel;
import com.dani3l11.carfleet.data.repository.RefuelRepository;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;

import java.sql.Ref;


public class RefuelManageFragment extends Fragment {


    public static final String TAG = "RefuelManageFragment";
    private TextInputEditText tietGasStation;
    private TextInputEditText tietDirection;
    private TextInputEditText tietRefuelQuantity;
    private TextInputEditText tietDate;
    private TextInputEditText tietPrice;
    private RefuelRepository repository;
    private FloatingActionButton fabAdd;
    private Refuel refuel;






    public static RefuelManageFragment newInstance(Bundle bundle) {
        RefuelManageFragment fragment = new RefuelManageFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_refuel_manage, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tietGasStation = view.findViewById(R.id.tiedGasStationRefuel);
        tietDirection = view.findViewById(R.id.tiedDirecctionRefuel);
        tietRefuelQuantity = view.findViewById(R.id.tiedRefuelQuantity);
        tietDate = view.findViewById(R.id.tiedDateRefuel);
        tietPrice = view.findViewById(R.id.tiedPriceRefuel);
        fabAdd = view.findViewById(R.id.fabManageRefuel);
        repository = new RefuelRepository();

        InitFab();

       Bundle bundle = getArguments();
        if (bundle != null){
            refuel = bundle.getParcelable(Refuel.TAG);

            tietGasStation.setText(refuel.getGasStation());
            tietDirection.setText(refuel.getUbication());
            tietRefuelQuantity.setText(refuel.getlRefuel().toString());
            tietDate.setText(refuel.getDate());
            tietPrice.setText(refuel.getPrice().toString());
        }
    }

    private void InitFab() {
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }


}
