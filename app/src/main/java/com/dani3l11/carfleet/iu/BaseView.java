package com.dani3l11.carfleet.iu;

public interface BaseView<T> {
    void setPresenter(T presenter);
    void onSuccess();
}
