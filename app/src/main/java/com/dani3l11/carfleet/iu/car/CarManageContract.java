package com.dani3l11.carfleet.iu.car;

import com.dani3l11.carfleet.data.model.Car;
import com.dani3l11.carfleet.iu.BaseView;

public interface CarManageContract {
    interface View extends BaseView<Presenter>{
        boolean onSuccessValidate();
    }

    interface Presenter{
        void validar(Car car);
        void add(Car car);
        void edit(Car car);
    }
}
