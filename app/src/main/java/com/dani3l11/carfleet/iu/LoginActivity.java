package com.dani3l11.carfleet.iu;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.Toast;

import com.dani3l11.carfleet.BindDriveApplication;
import com.dani3l11.carfleet.R;
import com.dani3l11.carfleet.data.model.Car;
import com.dani3l11.carfleet.data.model.GlobalData;
import com.dani3l11.carfleet.data.model.User;
import com.dani3l11.carfleet.data.model.UserAdmin;
import com.dani3l11.carfleet.data.repository.UserAdminRepository;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class LoginActivity extends AppCompatActivity {

    private String username, password;
    private ImageButton ibtFacebook;
    private Button btSignup;
    private ImageButton ibtTwitter;
    private ImageButton ibtGoogle;
    private Button btSignin;
    private Button btForgotPassword;
    private CheckBox cbRememberPassword;
    private ArrayList<User> userList;
    private TextInputEditText tietUser;
    private TextInputEditText tietPassword;
    private FirebaseAuth mAuth;
    private GoogleSignInClient mGoogleSignInClient;
    private static final int RC_SIGN_IN = 9001;
    private static final String TAG = "GoogleActivity";
    private SharedPreferences loginPreferences;
    private SharedPreferences.Editor loginPeferencesEditor;
    private Boolean saveLogin;


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e);
                // ...
            }
        }
    }

    // [START auth_with_google]
    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());
        // [START_EXCLUDE silent]
        //showProgressDialog();
        // [END_EXCLUDE]

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Toast.makeText(getApplicationContext(), "Te has logeado con google", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(LoginActivity.this, DashBoardActivity.class);

                            startActivity(intent);
                            FirebaseUser user = mAuth.getCurrentUser();



                        } else {
                            // If sign in fails, display a message to the user.
                            Toast.makeText(getApplicationContext(), "No te has logeado con googlee", Toast.LENGTH_SHORT).show();
                            Snackbar.make(findViewById(R.id.llLoginPadding), "Authentication Failed.", Snackbar.LENGTH_SHORT).show();
                        }

                        // [START_EXCLUDE]
                        //hideProgressDialog();
                        // [END_EXCLUDE]
                    }
                });
    }
    // [END auth_with_google]

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ibtFacebook = findViewById(R.id.ibtFacebook);
        ibtGoogle = findViewById(R.id.ibtGoogle);
        ibtTwitter = findViewById(R.id.ibtTwitter);
        btSignup = findViewById(R.id.btSingUp);
        btSignin = findViewById(R.id.btSingIn);
        btForgotPassword = findViewById(R.id.btForgotPassword);
        tietUser = findViewById(R.id.tietUser);
        tietPassword = findViewById(R.id.tietPassword);
        cbRememberPassword = findViewById(R.id.cbRememberPassword);

        // SHARED PREFERENCES REMEMBER PASSWORD

        loginPreferences = getSharedPreferences("loginPrefs", MODE_PRIVATE);
        loginPeferencesEditor = loginPreferences.edit();

        saveLogin = loginPreferences.getBoolean("saveLogin", false);
        if (saveLogin == true){
            tietUser.setText(loginPreferences.getString("username", ""));
            tietPassword.setText(loginPreferences.getString("password", ""));
            cbRememberPassword.setChecked(true);
        }

        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mAuth = FirebaseAuth.getInstance();

        /**
         * Este botón llevará a la activity de Registro
         */
        btSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

        //TODO: Dar funcionalidad al Botón de Facebook
        ibtFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Se ha pulsado el botón de Facebook", Toast.LENGTH_SHORT).show();
            }
        });

        ibtGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });



        ibtTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Se ha pulsado el botón de Twitter", Toast.LENGTH_SHORT).show();
            }
        });

        btSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                mAuth.signInWithEmailAndPassword(tietUser.getText().toString(), tietPassword.getText().toString()).addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            FirebaseUser user = mAuth.getCurrentUser();
                            // REMEMBER ME SHARED PREFERENCES
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(tietUser.getWindowToken(),0);

                            username = tietUser.getText().toString();
                            password = tietPassword.getText().toString();

                            if (cbRememberPassword.isChecked()){
                                loginPeferencesEditor.putBoolean("saveLogin", true);
                                loginPeferencesEditor.putString("username", username);
                                loginPeferencesEditor.putString("password", password);
                                loginPeferencesEditor.commit();
                            } else{
                                loginPeferencesEditor.clear();
                                loginPeferencesEditor.commit();
                            }

                            // Registro el usuario en el sistema
                            List<UserAdmin> users = UserAdminRepository.getInstance().getList();
                            for (UserAdmin userToFind : users){
                                if (userToFind.getEmail().equals(tietUser.getText().toString())){
                                    SaveUserGlobal(userToFind);
                                    break;
                                }
                            }

                            // Inicio la Activity DashBoardActivity
                            Intent intent = new Intent(LoginActivity.this, DashBoardActivity.class);
                            startActivity(intent);
                        } else {
                            Toast.makeText(getApplicationContext(), "Contraseña o email no válido", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });

        btForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Se ha pulsado el botón de Contraseña Olvidada", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void SaveUserGlobal(UserAdmin userToFind) {
        ((GlobalData)this.getApplication()).setUserLoged(userToFind);
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private boolean SignInCheck() {
        for (int i = 0; i < userList.size(); i++){
            if (tietUser.getText().toString().equals(userList.get(i).getEmail()) || tietUser.getText().toString().equals(userList.get(i).getUserName())){
                if (tietPassword.getText().toString().equals(userList.get(i).getPassword())){
                    return true;
                }
            }
        }
        return false;
    }
}
