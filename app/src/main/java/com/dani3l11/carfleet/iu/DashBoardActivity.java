package com.dani3l11.carfleet.iu;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.dani3l11.carfleet.PreferencesActivity;
import com.dani3l11.carfleet.R;
import com.dani3l11.carfleet.adapter.CarAdapter;
import com.dani3l11.carfleet.data.model.Car;
import com.dani3l11.carfleet.data.model.Refuel;
import com.dani3l11.carfleet.data.repository.CarRepository;
import com.dani3l11.carfleet.iu.car.CarListFragment;
import com.dani3l11.carfleet.iu.car.CarListPresenter;
import com.dani3l11.carfleet.iu.car.CarManageFragment;
import com.dani3l11.carfleet.iu.car.CarManagePresenter;
import com.dani3l11.carfleet.iu.drivers.DriverMainActivity;
import com.dani3l11.carfleet.iu.profile.ProfileFragment;
import com.dani3l11.carfleet.iu.refuel.RefuelListFragment;
import com.dani3l11.carfleet.iu.refuel.RefuelListPresenter;
import com.dani3l11.carfleet.iu.refuel.RefuelManageFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;

public class DashBoardActivity extends AppCompatActivity implements DashBoardFragment.OnDashBoardListener, CarListFragment.OnManageCarListener, NavigationView.OnNavigationItemSelectedListener, RefuelListFragment.OnManageRefuelListener, ProfileFragment.OnFragmentInteractionListener {

    private FragmentTransaction fragmentTransaction;
    private DashBoardFragment dashBoardFragment;
    private AboutUsFragment aboutUsFragment;
    private CarListFragment carListFragment;
    private CarManageFragment carManageFragment;
    private RefuelManageFragment refuelManageFragment;
    private RefuelListPresenter refuelListPresenter;
    private CarListPresenter carListPresenter;
    private RefuelListFragment refuelListFragment;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private CarManagePresenter carManagePresenter;
    private CarListFragment carListFragment2;
    private ProfileFragment profileFragment;
    final RefuelListFragment fr_refuel = new RefuelListFragment();
    final CarListFragment fr_car = new CarListFragment();
    Fragment active = fr_refuel;
    final FragmentManager fm = getSupportFragmentManager();

    private com.github.clans.fab.FloatingActionButton fabiCar;
    private com.github.clans.fab.FloatingActionButton fabiMaintenance;
    private com.github.clans.fab.FloatingActionButton fabiRefuel;

    static CarAdapter carAdapter;

    View header;

    public static void PasarCarAdapter(CarAdapter carAdapterA){
        carAdapter = carAdapterA;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        //toolbar.inflateMenu(R.menu.navigation_history);

        // Lateral Menu
        navigationView = findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
        header = navigationView.getHeaderView(0);

        fabiCar = findViewById(R.id.fabiCar);
        fabiRefuel = findViewById(R.id.fabiRefuel);


        drawerLayout = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        //showDashBoard();

        refuelListPresenter = new RefuelListPresenter(fr_refuel);
        fr_refuel.setPresenter(refuelListPresenter);
        fm.beginTransaction().add(R.id.main_container, fr_refuel, "1").commit();


        BottomNavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                selectFragment(menuItem);
                return true;
            }
        });


        fabiCar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onManageCar(null);
            }
        });

        fabiRefuel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onManageRefuel(null);
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.sort_car_menu, menu);
        return true;
    }

    private void selectFragment(MenuItem menuItem) {
        //init corresponding fragment
        switch (menuItem.getItemId()){
            case R.id.nav_history:
                refuelListPresenter = new RefuelListPresenter(fr_refuel);
                fr_refuel.setPresenter(refuelListPresenter);
                fm.beginTransaction().replace(R.id.main_container, fr_refuel, "1").commit();
                break;
                //TODO: INICIALITE PRESENTER OF CAR MANAGE
            case R.id.nav_vehicles:
                FragmentManager fragmentManager = getSupportFragmentManager();
                carListFragment = (CarListFragment) fragmentManager.findFragmentByTag(CarListFragment.TAG);
                if (carListFragment == null){
                    carListFragment = (CarListFragment) carListFragment.newInstance(null);
                    carListPresenter = new CarListPresenter(fr_car);
                    fr_car.setPresenter(carListPresenter);
                    fm.beginTransaction().replace(R.id.main_container, fr_car, "2").commit();
                }


                active = fr_car;
                break;
        }
    }


    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)){
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void showDashBoard() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        dashBoardFragment = new DashBoardFragment();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(android.R.id.content, dashBoardFragment, DashBoardFragment.TAG);
        fragmentTransaction.commit();
    }

    @Override
    public void onDashBoardCarList() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        carListFragment = (CarListFragment) fragmentManager.findFragmentByTag(CarListFragment.TAG);
        if (carListFragment == null){
            carListFragment = (CarListFragment) carListFragment.newInstance(null);
            fragmentManager.beginTransaction().replace(android.R.id.content, carListFragment, CarListFragment.TAG).addToBackStack(null).commit();
        }
    }

    @Override
    public void onDashBoardRefuelList() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        refuelListFragment = (RefuelListFragment) fragmentManager.findFragmentByTag(RefuelListFragment.TAG);
        if (refuelListFragment == null){
            refuelListFragment = new RefuelListFragment();
            fragmentManager.beginTransaction().replace(android.R.id.content, refuelListFragment, RefuelListFragment.TAG).addToBackStack(null).commit();
        }
    }

    @Override
    public void onDashBoardAboutUs() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        aboutUsFragment = new AboutUsFragment();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(android.R.id.content, aboutUsFragment, AboutUsFragment.TAG);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }


    @Override
    public void onManageCar(Car car) {
        carManageFragment = (CarManageFragment) getSupportFragmentManager().findFragmentByTag(CarManageFragment.TAG);
        if (carManageFragment == null){
            Bundle bundle = null;
            if(car != null){
                bundle = new Bundle();
                bundle.putParcelable(Car.TAG, car);
            }
            carManageFragment = carManageFragment.newInstance(bundle);

            carManagePresenter = new CarManagePresenter(carManageFragment);
            carManageFragment.setPresenter(carManagePresenter);

            fragmentTransaction = getSupportFragmentManager().beginTransaction();


            fragmentTransaction.replace(android.R.id.content, carManageFragment, CarManageFragment.TAG);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();

        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()){
            case R.id.navigation_drivers:
                Intent intentDrivers = new Intent(DashBoardActivity.this, DriverMainActivity.class);
                startActivity(intentDrivers);
                break;
            case R.id.navigation_aboutus:
                onDashBoardAboutUs();
                break;
            case R.id.navigation_logout:
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.navigation_settings:
                Intent intentPreferences = new Intent(DashBoardActivity.this, PreferencesActivity.class);
                startActivity(intentPreferences);
                break;
        }
        return true;
    }

    public void sortName(MenuItem item) {
        //CarRepository.getRepository().SortCarByName();
        carAdapter.notifyDataSetChanged();
    }

    public void sortFuel(MenuItem item) {
        //CarRepository.getRepository().SortCarByFuel();
        carAdapter.notifyDataSetChanged();
    }

    @Override
    public void onManageRefuel(Refuel refuel) {
        refuelManageFragment = (RefuelManageFragment) getSupportFragmentManager().findFragmentByTag(RefuelManageFragment.TAG);
        if (refuelManageFragment == null){
            Bundle bundle = null;
            if(refuel != null){
                bundle = new Bundle();
                bundle.putParcelable(Refuel.TAG, refuel);
            }
            refuelManageFragment = refuelManageFragment.newInstance(bundle);


            fragmentTransaction = getSupportFragmentManager().beginTransaction();


            fragmentTransaction.replace(android.R.id.content, refuelManageFragment, RefuelManageFragment.TAG);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();

        }
    }

    @Override
    public void onFragmentInteraction() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        profileFragment = new ProfileFragment();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(android.R.id.content, profileFragment, ProfileFragment.TAG);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}
