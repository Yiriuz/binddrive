package com.dani3l11.carfleet.iu.refuel;

import com.dani3l11.carfleet.data.model.Refuel;
import com.dani3l11.carfleet.data.repository.RefuelRepository;

public class RefuelListPresenter implements RefuelListContract.Presenter {

    private RefuelListContract.View view;

    public RefuelListPresenter(RefuelListContract.View view){
        this.view = view;
    }

    @Override
    public void delete(Refuel refuel) {
        RefuelRepository.getInstance().delete(refuel);
        view.onSuccessDelete();
    }
}
