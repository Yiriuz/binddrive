package com.dani3l11.carfleet.iu;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.dani3l11.carfleet.R;
import com.dani3l11.carfleet.data.model.GlobalData;
import com.dani3l11.carfleet.data.model.UserAdmin;
import com.google.firebase.auth.FirebaseAuth;

public class DashBoardFragment extends Fragment {

    public static final String TAG = "DashBoardFragment";
    private ImageButton ibCar;
    private ImageButton ibOil;
    private ImageButton ibAboutUs;
    private ImageButton ibLogout;
    private OnDashBoardListener onDashBoardListener;
    private TextView tvNameHeader;


    public interface OnDashBoardListener{
        void onDashBoardAboutUs();
        void onDashBoardCarList();
        void onDashBoardRefuelList();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onDashBoardListener = (OnDashBoardListener) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ibCar = view.findViewById(R.id.ibCar);
        ibOil = view.findViewById(R.id.ibOil);
        ibLogout = view.findViewById(R.id.ibLogout);
        ibAboutUs = view.findViewById(R.id.ibAboutUs);
        tvNameHeader = view.findViewById(R.id.tvNameUserNavHeader);

        UserAdmin userLoged = ((GlobalData)this.getContext()).getUserLoged();

        tvNameHeader.setText(userLoged.getEmail());

        /*ibCar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDashBoardListener.onDashBoardCarList();
            }
        });

        ibOil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDashBoardListener.onDashBoardRefuelList();
            }
        });

        ibAboutUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDashBoardListener.onDashBoardAboutUs();
            }
        });

        ibLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
            }
        });*/






    }


}
