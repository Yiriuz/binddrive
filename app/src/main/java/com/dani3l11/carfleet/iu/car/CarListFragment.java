package com.dani3l11.carfleet.iu.car;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dani3l11.carfleet.R;
import com.dani3l11.carfleet.adapter.CarAdapter;
import com.dani3l11.carfleet.data.model.Car;
import com.dani3l11.carfleet.iu.BaseDialogFragment;
import com.dani3l11.carfleet.iu.DashBoardActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class CarListFragment extends Fragment implements CarListContract.View, BaseDialogFragment.OnFinishDialogListener {

    public static final String TAG = "CarListFragment" ;
    private static final int CODE_DELETE = 200;
    private RecyclerView rvCar;
    private FloatingActionButton fabNewCar;
    private CarAdapter.OnManageCarListener listenerAdapter;
    private OnManageCarListener listenerManage;
    private CarAdapter adapter;
    private CarListContract.Presenter presenter;
    private Car deleted;

    public static Fragment newInstance(Bundle bundle){
        CarListFragment carListFragment = new CarListFragment();
        if (bundle != null)
            carListFragment.setArguments(bundle);
        return carListFragment;
    }



    public interface OnManageCarListener {
        void onManageCar(Car car);
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.load();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.load();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list_car, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvCar = view.findViewById(R.id.rvCar);
        getActivity().setTitle("Cars");
        //fabNewCar = view.findViewById(R.id.fabNewCar);

        initListCar();
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            listenerManage = (OnManageCarListener) context;
        } catch (ClassCastException e){
            throw new ClassCastException(context.toString() + "must implement OnManageCarListener");
        }
    }

    private void initListCar() {
        InitListenerAdapter();

        adapter = new CarAdapter();
        CarManageFragment.PasarCarAdapter(adapter);
        DashBoardActivity.PasarCarAdapter(adapter);
        adapter.setOnManageCar(listenerAdapter);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        rvCar.setLayoutManager(linearLayoutManager);

        rvCar.setAdapter(adapter);


    }

    private void InitListenerAdapter() {
        listenerAdapter = new CarAdapter.OnManageCarListener() {
            @Override
            public void onEditCar(Car car) {
                listenerManage.onManageCar(car);
            }

            @Override
            public void onDeleteCar(Car car) {
                showDialogFragment(car);

            }
        };
    }

    private void showDialogFragment(Car car) {
        Bundle bundle = new Bundle();
        bundle.putString(BaseDialogFragment.TITLE, "Eliminar");
        bundle.putString(BaseDialogFragment.MESSAGE, "¿Esta seguro de que quiere eliminar el coche " + car.getName() + "?");
        BaseDialogFragment baseDialogFragment = BaseDialogFragment.newInstance(bundle);
        baseDialogFragment.setTargetFragment(CarListFragment.this, CODE_DELETE);
        baseDialogFragment.show(getFragmentManager(), BaseDialogFragment.TAG);
        deleted = car;
    }

    @Override
    public void onFinishDialog() {
        presenter.delete(deleted);
    }


    @Override
    public void onSuccessDelete() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onSuccess(List<Car> carlist) {
        adapter.clear();
        adapter.load(carlist);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void setPresenter(CarListContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void onSuccess() {
        //presenter.load();
    }
}
