package com.dani3l11.carfleet.iu.refuel;

import com.dani3l11.carfleet.data.model.Refuel;
import com.dani3l11.carfleet.iu.BaseView;

public interface RefuelListContract {
    interface View extends BaseView<Presenter>{
        void onSuccessDelete();
    }

    interface Presenter{
        void delete(Refuel refuel);
    }
}
