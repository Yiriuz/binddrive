package com.dani3l11.carfleet.iu;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Application;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.dani3l11.carfleet.R;
import com.dani3l11.carfleet.data.model.GlobalData;
import com.dani3l11.carfleet.data.model.UserAdmin;
import com.dani3l11.carfleet.data.repository.UserAdminRepository;
import com.dani3l11.carfleet.util.CommonUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.util.List;

public class RegisterActivity extends AppCompatActivity {

    private Button btBack;
    private TextInputEditText tietEmail;
    private TextInputEditText tietUserName;
    private TextInputEditText tietPassword;
    private TextInputEditText tietPassword2;
    private TextInputEditText tietName;
    private Button btSignUp;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        btBack = findViewById(R.id.btBack);
        btSignUp = findViewById(R.id.btSingUpRegister);
        tietEmail = findViewById(R.id.tietEmailRegister);
        tietUserName = findViewById(R.id.tietUserRegister);
        tietPassword = findViewById(R.id.tietPasswordRegister);
        tietPassword2 = findViewById(R.id.tietPasswordRegisterAgain);
        tietName = findViewById(R.id.tietNameRegister);
        mAuth = FirebaseAuth.getInstance();


        btBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validatePassword()){
                    mAuth.createUserWithEmailAndPassword(tietEmail.getText().toString(), tietPassword.getText().toString()).addOnCompleteListener(RegisterActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(task.isSuccessful()){
                                Toast.makeText(getApplicationContext(), "Buen trabajao Te has registrado en la base de datos", Toast.LENGTH_SHORT).show();
                                RegistroBaseDatos();
                                finish();
                            } else {
                                Toast.makeText(getApplicationContext(), "No ha sido posible registrarte", Toast.LENGTH_SHORT).show();
                            }

                        }
                    });
                }
            }
        });
    }

    private void RegistroBaseDatos() {
        List<UserAdmin> users = UserAdminRepository.getInstance().getList();
        UserAdmin userTmp = new UserAdmin(tietEmail.getText().toString(), tietUserName.getText().toString(), tietPassword.getText().toString(),tietName.getText().toString(), null, null, null, null);
        users.add(userTmp);
    }

    private boolean validatePassword() {
        if (!tietPassword.getText().toString().equals(tietPassword2.getText().toString()))
        {
            ShowPasswordError("Las contraseñas son diferentes");
            return false;
        }




        return true;

    }

    private void ShowPasswordError(String message) {
        Snackbar snackbar = Snackbar.make(this.findViewById(android.R.id.content), message, Snackbar.LENGTH_SHORT);
        View view = snackbar.getView();
        TextView tv = view.findViewById(com.google.android.material.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
        snackbar.show();
    }
}
