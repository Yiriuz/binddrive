package com.dani3l11.carfleet.iu.drivers;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.dani3l11.carfleet.R;

public class DriverMainActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    private FragmentTransaction fragmentTransaction;
    private DriverListFragment driverListFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_main);

        Toolbar toolbar = findViewById(R.id.toolbarBase);

        toolbar.setTitle(R.string.drivers);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbarBase));

        drawerLayout = findViewById(R.id.dlBase);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        StartListFragment();
    }

    private void StartListFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        driverListFragment = new DriverListFragment();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.flConent, driverListFragment, DriverListFragment.TAG);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}
