package com.dani3l11.carfleet.iu.car;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.Fragment;

import com.dani3l11.carfleet.BindDriveApplication;
import com.dani3l11.carfleet.R;
import com.dani3l11.carfleet.adapter.CarAdapter;
import com.dani3l11.carfleet.data.model.Car;
import com.dani3l11.carfleet.data.repository.CarRepository;
import com.dani3l11.carfleet.iu.DashBoardActivity;
import com.dani3l11.carfleet.iu.SplashActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.internal.ParcelableSparseArray;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Date;
import java.util.Random;

public class CarManageFragment extends Fragment implements CarManageContract.View {

    public static final String TAG = "CarManageFragment";
    private TextInputEditText tiedName;
    private TextInputEditText tiedKilometers;
    private TextInputEditText tiedBrand;
    private TextInputEditText tiedModel;
    private TextInputEditText tiedFuel;
    private FloatingActionButton fabManageCar;
    private CarRepository repository;
    private Car car;
    private CarManageContract.Presenter presenter;
    private Context activityconext;

    static CarAdapter carAdapter;

    public static void PasarCarAdapter(CarAdapter carAdapterA){
        carAdapter = carAdapterA;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_manage_car, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tiedName = view.findViewById(R.id.tiedNameCar);
        tiedBrand = view.findViewById(R.id.tiedBrand);
        tiedModel = view.findViewById(R.id.tiedModel);
        tiedKilometers = view.findViewById(R.id.tiedKilometers);
        tiedFuel = view.findViewById(R.id.tiedFuelManage);
        fabManageCar = view.findViewById(R.id.fabManageCar);




        initFab();

        Bundle bundle = getArguments();
        if (bundle != null){
            car = bundle.getParcelable(Car.TAG);

            tiedName.setText(car.getName());
            tiedBrand.setText(car.getBrand());
            tiedModel.setText(car.getModel());
            tiedKilometers.setText(car.getKilometers().toString());
            tiedFuel.setText(car.getFuel().toString());
}
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        activityconext = context;
    }

    private void showDatePickerDialog() {
        DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener(){
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                final String selectedDate = dayOfMonth + " / " + (month+1) + " / " + year;
            }
        });
        newFragment.show(getActivity().getSupportFragmentManager(), "datePicker");

    }

    private void initFab() {
        fabManageCar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getArguments() != null){
                    car = getCar();
                    presenter.edit(car);
                } else{
                    car = getCar();
                    presenter.add(car);
                }

            }
        });
    }

    private Car getCar() {
        Car car = new Car();
        car.setName(tiedName.getText().toString());
        car.setBrand(tiedBrand.getText().toString());
        car.setModel(tiedModel.getText().toString());
        car.setKilometers(tiedKilometers.getText().toString());
        car.setFuel(Integer.parseInt(tiedFuel.getText().toString()));
        return car;
    }

    public static CarManageFragment newInstance(Bundle bundle) {
        CarManageFragment fragment = new CarManageFragment();
        if (bundle != null)
            fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public boolean onSuccessValidate() {
        return false;
    }

    @Override
    public void setPresenter(CarManageContract.Presenter presenter) {
        this.presenter = presenter;
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onSuccess() {
        getActivity().onBackPressed();

        Intent intent = new Intent(activityconext, DashBoardActivity.class);
        Bundle bundle = new Bundle();
        Car car = new Car();

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("Notificaciones", true);
        intent.putExtras(bundle);


        // Random de getAcitvity sirve para que no te carge los datos de la última.
        PendingIntent pendingIntent = PendingIntent.getActivity(activityconext, new Random().nextInt(100), intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification.Builder builder = new Notification.Builder(activityconext, BindDriveApplication.CHANNEL_ID)
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.ic_car)
                .setContentText("Se ha realizado una modificación en los coches")
                .setContentTitle("Modificación Coches")
                .setContentIntent(pendingIntent);

        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(activityconext);

        Random rnd = new Random();
        // Este random sirve para que no se superponga con las otras.
        int id = rnd.nextInt(100);

        notificationManagerCompat.notify(id, builder.build());

        //SendNotification();
        //carAdapter.notifyDataSetChanged();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void SendNotification() {

    }


}
