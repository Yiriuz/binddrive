package com.dani3l11.carfleet.iu.car;

import com.dani3l11.carfleet.data.model.Car;
import com.dani3l11.carfleet.data.repository.CarRepository;

import java.util.List;

public class CarListPresenter implements CarListContract.Presenter {

    private CarListContract.View view;

    public CarListPresenter(CarListContract.View view){
        this.view = view;
    }


    @Override
    public void delete(Car car) {
        CarRepository.getRepository().delete(car);
        view.onSuccessDelete();
    }

    @Override
    public void load() {
        List<Car> carlist = CarRepository.getRepository().getList();
        view.onSuccess(carlist);
    }

    public interface OnSuccessListener{
        void onSuccess(List<Car> cars);
    }
}
