package com.dani3l11.carfleet.iu.refuel;

import com.dani3l11.carfleet.data.model.Refuel;

public class RefuelManagePresenter implements RefuelManageContract.Presenter {

    private RefuelManageContract.View view;

    public RefuelManagePresenter(RefuelManageContract.View view){
        this.view = view;
    }

    @Override
    public void validar(Refuel refuel) {

    }

    @Override
    public void add(Refuel refuel) {

    }

    @Override
    public void edit(Refuel refuel) {

    }
}
