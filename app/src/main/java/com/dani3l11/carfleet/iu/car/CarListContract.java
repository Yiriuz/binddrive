package com.dani3l11.carfleet.iu.car;

import com.dani3l11.carfleet.data.model.Car;
import com.dani3l11.carfleet.iu.BaseView;

import java.util.List;

public interface CarListContract {
    interface View extends BaseView<Presenter> {
        void onSuccessDelete();
        void onSuccess(List<Car> carlist);
    }

    interface Presenter{
        void delete(Car car);
        void load();
    }
}
