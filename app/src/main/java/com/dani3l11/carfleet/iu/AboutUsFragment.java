package com.dani3l11.carfleet.iu;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.fonts.Font;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.dani3l11.carfleet.R;
import com.danielstone.materialaboutlibrary.ConvenienceBuilder;
import com.danielstone.materialaboutlibrary.MaterialAboutFragment;
import com.danielstone.materialaboutlibrary.items.MaterialAboutActionItem;
import com.danielstone.materialaboutlibrary.items.MaterialAboutItemOnClickAction;
import com.danielstone.materialaboutlibrary.items.MaterialAboutTitleItem;
import com.danielstone.materialaboutlibrary.model.MaterialAboutCard;
import com.danielstone.materialaboutlibrary.model.MaterialAboutList;
import com.mikepenz.iconics.IconicsColor;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.iconics.IconicsSize;
import com.mikepenz.iconics.typeface.library.fontawesome.FontAwesome;


public class AboutUsFragment extends MaterialAboutFragment {

    public static final String TAG = "AboutUsFragment";


    @Override
    protected MaterialAboutList getMaterialAboutList(final Context context) {

        MaterialAboutCard.Builder appCardBuilder = new MaterialAboutCard.Builder();

        // Add items card

        appCardBuilder.addItem(new MaterialAboutTitleItem.Builder()
                .text("BindDrive")
                .desc("© 2019 BindDrive")
                .icon(R.drawable.icon_logo).build());

        appCardBuilder.addItem(ConvenienceBuilder.createVersionActionItem(context,
                new IconicsDrawable(context)
                        .icon(FontAwesome.Icon.faw_info_circle)
                        .color(IconicsColor.colorInt(Color.GRAY))
                        .size(IconicsSize.dp(18)),
                "Version",
                false));

        //AUTHOR
        MaterialAboutCard.Builder authorCardBuilder = new MaterialAboutCard.Builder();
        authorCardBuilder.title("Author");

        authorCardBuilder.addItem(new MaterialAboutActionItem.Builder()
            .text("Antonio Daniel García Guerrero")
            .subText("España, Málaga")
            .icon(new IconicsDrawable(context)
                .icon(FontAwesome.Icon.faw_user_tie)
                .color(IconicsColor.colorInt(Color.GRAY))
                .size(IconicsSize.dp(18))).build());

        authorCardBuilder.addItem(new MaterialAboutActionItem.Builder()
            .text("GitLab")
            .icon(new IconicsDrawable(context)
                .icon(FontAwesome.Icon.faw_gitlab)
                    .color(IconicsColor.colorInt(Color.GRAY))
                    .size(IconicsSize.dp(18)))
                .setOnClickAction(ConvenienceBuilder.createWebsiteOnClickAction(context, Uri.parse("https://gitlab.com/Dani3l11"))).build());

        // Contact Information

        MaterialAboutCard.Builder contactCardBuilder = new MaterialAboutCard.Builder();
        contactCardBuilder.title("Contact");

        contactCardBuilder.addItem(ConvenienceBuilder.createWebsiteActionItem(context,
                new IconicsDrawable(context)
                    .icon(FontAwesome.Icon.faw_globe_americas)
                        .color(IconicsColor.colorInt(Color.GRAY))
                        .size(IconicsSize.dp(18)),
                "Visit Website",
                true,
                Uri.parse("https://google.com")));


        contactCardBuilder.addItem(ConvenienceBuilder.createEmailItem(context,
                new IconicsDrawable(context)
                        .icon(FontAwesome.Icon.faw_envelope)
                        .color(IconicsColor.colorInt(Color.GRAY))
                        .size(IconicsSize.dp(18)),
                "Send an email",
                true,
                "garciaantoniodaniel11@gmail.com",
                "Question concerning BindDrive"));



        return new MaterialAboutList(appCardBuilder.build(), authorCardBuilder.build(), contactCardBuilder.build());
    }

    @Override
    protected int getTheme() {
        return R.style.AppTheme_MaterialAboutActivity_Fragment;
    }
/**
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragement_aboutus, container, false);
    }
    */

}
