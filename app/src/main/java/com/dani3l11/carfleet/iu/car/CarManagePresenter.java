package com.dani3l11.carfleet.iu.car;

import com.dani3l11.carfleet.data.model.Car;
import com.dani3l11.carfleet.data.repository.CarRepository;

public class CarManagePresenter implements CarManageContract.Presenter {
    private CarManageContract.View view;

    public CarManagePresenter(CarManageContract.View view){
        this.view = view;
    }

    @Override
    public void validar(Car car) {

    }

    @Override
    public void add(Car car) {
        CarRepository.getRepository().insert(car);
        //CarRepository.getRepository().SortCarByName();
        view.onSuccess();
    }

    @Override
    public void edit(Car car) {
        CarRepository.getRepository().update(car);
        //CarRepository.getRepository().SortCarByName();
        view.onSuccess();
    }
}
