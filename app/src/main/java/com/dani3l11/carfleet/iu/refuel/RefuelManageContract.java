package com.dani3l11.carfleet.iu.refuel;

import com.dani3l11.carfleet.data.model.Refuel;
import com.dani3l11.carfleet.iu.BaseView;

public interface RefuelManageContract {
    interface View extends BaseView<Presenter>{
        boolean onSuccessValidate();
    }

    interface Presenter{
        void validar(Refuel refuel);
        void add(Refuel refuel);
        void edit(Refuel refuel);
    }
}
